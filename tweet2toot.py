import os
from mastodon import Mastodon
import psycopg2
import sys
import time
import requests
import shutil
import tweepy
from tweepy.errors import (
    BadRequest, Forbidden, HTTPException, TooManyRequests, TwitterServerError,
    Unauthorized
)
from tweepy import TweepyException
import logging
import pdb

logger = logging.getLogger()

def parse_url(toot_text):

    sub_str = 'http'
    find_link = toot_text.find(sub_str)
    if find_link != -1:

        tuit_text = toot_text[:toot_text.index(sub_str)]

    else:

        tuit_text = toot_text

    return tuit_text

def get_toot_id(tweet_id):

    toot_id = 0

    try:

        conn = None

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute('select toot_id, tweet_id from id where tweet_id=(%s)', (tweet_id,))

        row = cur.fetchone()

        if row != None:

            toot_id = row[0]

        cur.close()

        return(toot_id)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def write_db(toot_id, tweet_id):

    sql_insert_ids = 'INSERT INTO id(toot_id, tweet_id) VALUES (%s,%s)'

    conn = None

    try:

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(sql_insert_ids, (toot_id, tweet_id))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def write_image(image_url):

    if not os.path.exists('images'):
        os.makedirs('images')
    filename = image_url.split("/") [-1]
    r = requests.get(image_url, stream = True)
    r.raw.decode_content = True
    with open('images/' + filename, 'wb') as f:
        shutil.copyfileobj(r.raw, f)

    return filename

def oauth2():

    auth = tweepy.OAuthHandler(api_key, api_key_secret)

    try:
        apiv1 = tweepy.API(auth)
        logged_in = True
    except Exception as e:
        logger.error("Error creating API", exc_info=True)
        raise e
    logger.info("API created")
    return (apiv1, logged_in)

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id=uc_client_id,
        client_secret=uc_client_secret,
        access_token=uc_access_token,
        api_base_url='https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers = {'Authorization': 'Bearer %s'%uc_access_token}

    return (mastodon, mastodon_hostname)

def db_config():

    # Load db configuration from config file
    db_config_filepath = "config/db_config.txt"
    feeds_db =  get_parameter("feeds_db", db_config_filepath)
    feeds_db_user =  get_parameter("feeds_db_user", db_config_filepath)
    
    return (feeds_db, feeds_db_user)

def twitter_config():

    twitter_config_filepath = "config/keys_config.txt"
    api_key = get_parameter("api_key", twitter_config_filepath)
    api_key_secret = get_parameter("api_key_secret", twitter_config_filepath)
    twitter_account_username = get_parameter("twitter_account_username", twitter_config_filepath)

    return(api_key, api_key_secret, twitter_account_username)

def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = mastodon()

    feeds_db, feeds_db_user = db_config()

    api_key, api_key_secret, twitter_account_username = twitter_config()

    logged_in = False

    # check new tweets

    if not logged_in:

        apiv1, logged_in = oauth2()

    try:

        user_id = apiv1.get_user(screen_name=twitter_account_username).id

        print(f'Authentication succesfull!')

        user_tweets = apiv1.user_timeline(user_id=user_id, tweet_mode='extended')

    except tweepy.errors.Unauthorized as non_auth_error:

        sys.exit(f'\n{non_auth_error}\n\nCheck your Twitter API key & secret!')

    except tweepy.errors.TweepyException as tweepy_exception:

        sys.exit(f'\n{tweepy_exception}')

    for tweet in reversed(user_tweets):

        if not tweet.retweeted and tweet.entities['user_mentions'] == []:

            tweet_id = tweet.id

            toot_id = get_toot_id(tweet_id)

            if toot_id == 0:

                print(f'tweet id: {tweet.id}, {tweet.full_text}')

                is_reply = False

                is_media = False

                toot_text = tweet.full_text

                if tweet.in_reply_to_status_id != None and tweet.in_reply_to_user_id == user_id:

                    tweet_reply_id = tweet.in_reply_to_status_id

                    reply_toot_id = get_toot_id(tweet_reply_id)

                    if reply_toot_id != 0:

                        is_reply = True

                    else:

                        reply_toot_id = None

                else:

                    reply_toot_id = None

                if 'media' in tweet.entities:

                    is_media = True

                    toot_text = toot_text.replace(tweet.entities['media'][0]['url'],'')

                    images_id_lst = []

                    media_qty = len(tweet.entities['media'])

                    i = 0
                    while i < media_qty:

                        media_url = tweet.entities['media'][0]['media_url']
                        image_filename = write_image(media_url)
                        image_id = mastodon.media_post('images/'+ image_filename, "image/png", description='twitter image').id
                        images_id_lst.append(image_id)

                        i += 1

                if len(tweet.entities['urls']) > 0:

                    toot_text = parse_url(toot_text)

                    url = tweet.entities['urls'][0]['expanded_url']

                    toot_text = f'{toot_text}\n{url}' 

                toot_id = mastodon.status_post(toot_text, in_reply_to_id=reply_toot_id, media_ids=images_id_lst if is_media else None).id

                write_db(toot_id, tweet_id)

                time.sleep(2)

    print('Done.')
