import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up tweet2toot parameters...")
    print("\n")
    feeds_db = input("feeds db name: ")
    feeds_db_user = input("feeds db user: ")

    with open(file_path, "w") as text_file:
        print("feeds_db: {}".format(feeds_db), file=text_file)
        print("feeds_db_user: {}".format(feeds_db_user), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

def create_index(db, db_user, table, index, sql_index):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating index.. "+index)
    # Create the table in PostgreSQL database
    cur.execute(sql_index)

    conn.commit()
    print("Index "+index+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()
#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
feeds_db = get_parameter("feeds_db", config_filepath)
feeds_db_user = get_parameter("feeds_db_user", config_filepath)

############################################################
# create database
############################################################

conn = None

try:

  conn = psycopg2.connect(dbname='postgres',
      user=feeds_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + feeds_db + ". Please wait...")

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(feeds_db))
      )
  print("Database " + feeds_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("db_config.txt")
  print("Exiting. Run db-setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("tweet2toot parameters saved to db-config.txt!")
  print("\n")

############################################################
# Create needed tables
############################################################

print("Creating table...")

########################################

db = feeds_db
db_user = feeds_db_user

table = "id"
index = "id_pkey"
sql = f'create table {table} (toot_id bigint, tweet_id bigint)'
create_table(db, db_user, table, sql)
sql_index = f'CREATE INDEX {index} ON {table} (toot_id, tweet_id)'
create_index(db, db_user, table, index, sql_index)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
