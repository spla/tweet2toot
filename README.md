# tweet2toot
Post to Mastodon all the tweets of the configured Twitter user account.  
tweet2toot will replicate all tweets and threads of the Twitter user of your choice.  

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account  
-   Twitter user account
-   Your personal Linux or Raspberry box.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed table in it.

3. Run `python setup.py` to input and save your Twitter's key and access tokens and the Twitter account username to be replicated. You can get your keys and tokens from [Twitter Developer Platform](https://developer.twitter.com/en/apply/user.html)  

4. Run `python mastodon-setup.py` to setup your Mastodon's bota ccount access tokens.

5. Use your favourite scheduling method to set `python tweet2toot.py` to run every minute.  
